package Lab;

import java.util.Scanner;

public class Lab7 {
	 public static void main(String[] args) {
		 			Q1();
		 			System.out.println(" ");
		 			Q2();

	    }
	 public static void Q1() {
		 int counter = 0;
		   do {
		     System.out.println("Counter :" + counter);
		     counter++;
		   } while (counter <= 5);
		 
	 }
	 public static void Q2() {
		 Scanner reader  = new Scanner(System.in);
	        int number;

	        System.out.println("Determine odd/even program");

	        do {
	            System.out.print("Enter odd number to exit loop: ");
	            number = reader.nextInt();

	            if (number % 2 == 0) {
	                System.out.println("You entered " + number + ", it's even.");
	            } else {
	                System.out.println("You entered " + number + ", it's odd.");
	            }

	        } while (number % 2 == 0);

	        System.out.println("Exited loop.");
	 }

}
